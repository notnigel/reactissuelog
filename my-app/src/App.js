import React from 'react';
import './App.css';
import ReactIssuesService from './ReactIssuesService'
import 'semantic-ui-less/semantic.less'


function App() {

  return (
    <div className="App">
      <ReactIssuesService />
    </div>
  );
}

export default App;
