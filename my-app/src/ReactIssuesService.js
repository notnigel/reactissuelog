import React from 'react';
import { Icon, Menu, Table, Button } from 'semantic-ui-react'
import _ from 'lodash'

/*
  This class isused for fetching the React issues from the git issue log.
*/
class ReactIssuesService extends React.Component {
  constructor() {
    super();
    this.state = {
      items: [],                  //List of issues
      isLoaded: false,            //content is loaded flag
      column: null,               //column which the table is sorted by
      direction: null,            //direction of which way the column is sortect
    };
  }

  /*
    Retrieve issues from github issue log.
  */
  componentDidMount() {
    fetch("https://api.github.com/repos/facebook/react/issues")
      .then(res => res.json())
      .then(
        result => {
          this.setState({
            isLoaded: true,
            items: result
          });
        },
        // Record the error message if an error is thrown
        error => {
          this.setState({
            isLoaded: true,
            error: error
          });
        }
      );
  }

  /*
    Function to sort the columns by which header is clicked.
  */
  handleSort = (clickedColumn) => () => {
    const { column, items, direction } = this.state

    // Set the column to the clicked coumn and sort it ascending
    if (column !== clickedColumn) {
      this.setState({
        column: clickedColumn,
        items: _.sortBy(items, [clickedColumn]),
        direction: 'ascending',
      })

      return
    }

    // If the column selected is the clicked column then reverse sort
    this.setState({
      items: items.reverse(),
      direction: direction === 'ascending' ? 'descending' : 'ascending',
    })
  }

  /*
    Render the table into the view
  */
  render() {
    const { error, isLoaded, items } = this.state;
    // Check for an error. Load the error message into the view if available.
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      console.log(this.state.items);
      return (
          <Table celled sortable>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell
                  sorted={this.state.column === 'id' ? this.state.direction : null}
                  onClick={this.handleSort('id')}
                >
                  Issue Number
                </Table.HeaderCell>
                <Table.HeaderCell
                  sorted={this.state.column === 'title' ? this.state.direction : null}
                  onClick={this.handleSort('title')}
                >
                  Title
                </Table.HeaderCell>
                <Table.HeaderCell
                  sorted={this.state.column === 'createdOn' ? this.state.direction : null}
                  onClick={this.handleSort('createdOn')}
                >
                  Created On
                </Table.HeaderCell>
                <Table.HeaderCell
                  sorted={this.state.column === 'updatedOn' ? this.state.direction : null}
                  onClick={this.handleSort('updatedOn')}
                >
                  Updated On
                </Table.HeaderCell>
                <Table.HeaderCell>Labels</Table.HeaderCell>
                <Table.HeaderCell
                  sorted={this.state.column === 'state' ? this.state.direction : null}
                  onClick={this.handleSort('state')}
                >
                  State
                </Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>
            {items.map(item =>
              <Table.Row key={item}>
                <Table.Cell>
                  {item.id}
                </Table.Cell>
                <Table.Cell>
                  {item.title}
                </Table.Cell>
                <Table.Cell>
                  {item.created_at}
                </Table.Cell>
                <Table.Cell>
                  {item.updated_at}
                </Table.Cell>
                <Table.Cell>
                  {item.labels.map(label => <Button>{label.name}</Button>)}
                </Table.Cell>
                <Table.Cell>
                  {item.state}
                </Table.Cell>
              </Table.Row>
            )}
            </Table.Body>

            <Table.Footer>
              <Table.Row>
                <Table.HeaderCell colSpan='3'>
                  <Menu floated='right' pagination>
                    <Menu.Item as='a' icon>
                      <Icon name='chevron left' />
                    </Menu.Item>
                    <Menu.Item as='a'>1</Menu.Item>
                    <Menu.Item as='a'>2</Menu.Item>
                    <Menu.Item as='a'>3</Menu.Item>
                    <Menu.Item as='a'>4</Menu.Item>
                    <Menu.Item as='a' icon>
                      <Icon name='chevron right' />
                    </Menu.Item>
                  </Menu>
                </Table.HeaderCell>
              </Table.Row>
            </Table.Footer>
          </Table>
      );
    }
  }
}

export default ReactIssuesService;
